---
title: "ESTP R: maps"
output: 
  pdf_document:
    includes:
      in_header: ../exercise.tex
params:
  answers: yes
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE, eval=TRUE}
library(dplyr)
library(tmap)
library(tmaptools)
data("World")
knitr::opts_chunk$set( eval=params$answers
                     , echo=params$answers
                     )
```

### Exercise 1: `tmap` 

In this exercise we'll explore thematic maps. Load the packages `tmap` and `tmaptools`. We will use the data set `World` provided by `tmap`.

a) Show a map of the world using the functions `tm_shape`, `tm_fill` and `tm_borders`. Create white borders and set the line width to 0.5.

```{r}
data("World")
tm_shape(World) + tm_fill() + tm_borders(lwd=0.5, col="white")
```

b) Create a choropleth (a colored thematic map) displaying the life expectancy of each country. Use the `virdis` color palette.\

```{r}
m <- tm_shape(World) + tm_fill("life_exp", palette = "viridis")
m
```

c) Add white country borders with 50% opacity.

```{r}
m + tm_borders("white", alpha=0.5)
```

d) Create a plot that shows the distribution of population densities. Adjust the plot to make it easier on the eye and easier and interpret.

```{r}
hist(log(World$pop_est))
```

e) Recreate the following bubble chart of estimated population sizes. Don't forget to add country borders.

```{r, eval=TRUE}
tm_shape(World) + tm_fill() + tm_borders(col="white", alpha=0.5) + tm_bubbles(size="pop_est", col="orange")
```

f) Recreate the following plot using `tm_facets`:

```{r,eval=TRUE}
tm_shape(World) +  tm_fill("pop_est_dens") + tm_facets(by="continent")
``` 

g) Create the same choropleth as in part c) of `pop_est_dens` for the Europian part of the data set. What's happening here? Explain.

```{r,eval=TRUE}
eu = World %>% filter(continent == "Europe")
tm_shape(eu) +  tm_fill("pop_est_dens") 
``` 

### Exercise 2: `sf`
In this exercise we will work with spatial data about the Netherlands which can be loaded with the command `data("NLD_muni")`.
a) 
Convert the `NLD_muni` data set to a sf data.frame and name the result `nld_sf`.

```{r}
library(sf)
data("NLD_muni")
nld_sf <- st_as_sf(NLD_muni)
```

b) Calculate the population density (inhabitants/km^2) and add this to the data set as a column named `pop_density`.

```{r}
nld_sf <- 
  nld_sf %>% 
  mutate(area = st_area(nld_sf)/1e6
        , pop_density = population / area
        )
```

d) Create a choropleth of `pop_density`

```{r}
tm_shape(nld_sf) +  tm_fill("pop_density")
```

e) Look at the distriution of population densities and assign colors to more suitable boundary values.

```{r}
tm_shape(nld_sf) +  tm_fill("pop_density", breaks = c(0, 250, 500, 750, 1000, 5000, 10000))
```

f) Create a histogram of `log(pop_density)`

```{r}
hist(log(unclass(nld_sf$pop_density)))
```

### Exercise 3: `stars`

a)
In this exercise we will use `stars`. Load the package and load the data set `land` from the `tmap` package. Print this `land` object to see what is in there.

```{r}
library(stars)
data("land")
land
```

b)
Create a world map of land cover using the `land` dataset. Add country polygon border on top.

```{r}
tm_shape(land) +
  tm_raster("cover") +
tm_shape(World) +
  tm_borders()
```
